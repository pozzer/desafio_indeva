class CreateSellersDailyGoals < ActiveRecord::Migration[5.1]
  def change
    create_table :sellers_daily_goals do |t|
      t.belongs_to :daily_goal, index: true
      t.belongs_to :seller, index: true

      t.timestamps
    end
  end
end
