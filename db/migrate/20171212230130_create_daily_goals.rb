class CreateDailyGoals < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_goals do |t|
      t.date :day
      t.numeric :amount, default: 0.0
      t.belongs_to :goal, index: true

      t.timestamps
    end
  end
end
