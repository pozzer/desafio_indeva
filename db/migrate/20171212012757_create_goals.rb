class CreateGoals < ActiveRecord::Migration[5.1]
  def change
    create_table :goals do |t|
      t.integer :month
      t.date :beginning
      t.date :ending
      t.numeric :amount
      t.belongs_to :store, index:true

      t.timestamps
    end
  end
end
