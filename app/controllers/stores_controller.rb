class StoresController < AppController
  def collection
    (get_collection_ivar || set_collection_ivar(search_ransack.page(params[:page]))).includes(:sellers)
  end

  def store_params
    params.require(:store).permit(:id, :name, sellers_attributes: [:id, :name, :_destroy]).merge(owner: current_owner)
  end

  protected
    def begin_of_association_chain
      current_owner
    end
end
