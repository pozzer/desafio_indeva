class ApplicationController < ActionController::Base
  respond_to :html
  before_action :authenticate_owner!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :get_owner_stores, if: :current_owner

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :password, :password_confirmation])
    end

    def get_owner_stores
      @owner_stores ||= current_owner.stores.where.not(id: 0)
    end
end
