class AppController < ApplicationController
  inherit_resources

  respond_to :html, :json

  helper_method :page_title, :t_action_name

  protect_from_forgery with: :exception

  add_breadcrumb ->(a){  a.page_title }, :collection_path
  add_breadcrumb :t_action_name

  def create
    create!{ collection_path }
  end

  def update
    update!{ collection_path }
  end

  protected

    def page_title
      t(controller_name.singularize, scope: 'activerecord.models').pluralize
    end

    def t_action_name
      t("views.#{controller_name}.#{action_name}", default: t("helpers.controller_actions.#{action_name}", default: action_name.camelize))
    end

    def search_ransack
      @q = end_of_association_chain.ransack(params[:q])
      @q.result
    end

    def collection
      (get_collection_ivar || set_collection_ivar(search_ransack.page(params[:page])))
    end
end
