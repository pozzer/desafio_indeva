class DailyGoalsController < AppController
  layout false
  respond_to :js
  belongs_to :store, :goal
  actions :edit, :update
  skip_before_action :verify_authenticity_token

  protected
    def daily_goal_params
      params.require(:daily_goal).permit(:id, :day, :amount, seller_ids:[]).merge(goal: @goal)
    end
end
