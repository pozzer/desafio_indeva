class GoalsController < AppController
  belongs_to :store

  protected
    def goal_params
      params.require(:goal).permit(:month, :beginning, :ending, :amount).merge(store: @store)
    end
end
