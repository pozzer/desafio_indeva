jQuery ->
  $('.goals.new, .goals.edit').ready ->
    priv =
      firstDay: null
      lastDay: null
      beginningPicker: $('#goal_beginning').pickadate("picker")
      endingPicker: $('#goal_ending').pickadate("picker")

    getYear = (date, month) ->
      if month >= date.getMonth() then date.getFullYear() else date.getFullYear() + 1

    disableDates = ->
      priv.beginningPicker.set 'enable', true
      priv.beginningPicker.set('disable', [ {
        from: priv.firstDay
        to: priv.lastDay
      } ]).set 'disable', 'flip'

      priv.endingPicker.set 'enable', true
      priv.endingPicker.set('disable', [ {
        from: priv.firstDay
        to: priv.lastDay
      } ]).set 'disable', 'flip'
      return

    selectDates = ->
      priv.beginningPicker.set 'select', priv.firstDay
      priv.endingPicker.set 'select', priv.lastDay
      return

    $('#goal_month').change(->
      month = @value - 1
      date = new Date
      priv.firstDay = new Date(getYear(date, month), month, 1)
      priv.lastDay = new Date(getYear(date, month), month + 1, 0)
      disableDates()
      selectDates()
      return
    ).trigger 'change'

    $('#goal_beginning').change(->
      priv.endingPicker.set("min", priv.beginningPicker.get('select'))
    ).trigger 'change'
