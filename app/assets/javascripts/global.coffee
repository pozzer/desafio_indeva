jQuery ->
  flash_message = $("#flash_message").text()
  if (flash_message.length > 0)
    showToast(JSON.parse(flash_message));
  $("[data-behavior='notifications-mark_all_read']").bind('click',(e)->
    e.preventDefault()
    App.notifications.mark_all_read()
  )

  $('document').ready ->
    $("#new_calendar").on("ajax:success", (e, data, status, xhr) ->
      $("#new_calendar").append xhr.responseText
    ).on "ajax:error", (e, xhr, status, error) ->
      $("#new_calendar").append "<p>ERROR</p>"

jQuery ->
  $("[type='email']").mask("A", {placeholder: 'xyz@xyz.com.br', translation: {"A": { pattern: /[\w\-.+@]/, recursive: true }} });
  $("[type='tel']").mask('(00) 0000-00009', {placeholder: '(00) 0000-00000'});
  $('[name$="[cep]"]').mask('00000-000', {placeholder: '00000-000'});
  $('[name$="[cpf_cnpj]"]').mask('000.000.000-00', {reverse: true, placeholder: "000.000.000-00"});
  $('[name$="[time]"]').mask('00:00', {reverse: true, placeholder: "08:00"});
  $('.date_picker').pickadate({format: "dd/mm/yyyy" });
  $('.money').mask('#.##0,00', {reverse: true});
  #$('[name$="[cpf_cnpj]"]').mask('00.000.000/0000-00', {reverse: true, placeholder: "00.000.000/0000-00"});


# see https://github.com/amsul/pickadate.js/issues/227
BAD_ATTR_RE = /\]_submit$/
jQuery ->
  $('form').submit (event) ->
    target = $(event.target)
    $.each target.find('input[type=hidden]'), (i, el) ->
      e = $(el)
      name = e.attr('name')
      if name.match(BAD_ATTR_RE)
        newName = name.replace(BAD_ATTR_RE, '_submit]')
        e.attr 'name', newName
      return
    return
  return
