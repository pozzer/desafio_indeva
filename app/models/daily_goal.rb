# == Schema Information
#
# Table name: daily_goals
#
#  id         :integer          not null, primary key
#  day        :date
#  amount     :decimal(, )      default(0.0)
#  goal_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_daily_goals_on_goal_id  (goal_id)
#

class DailyGoal < ApplicationRecord
  belongs_to :goal, required: true

  has_many :sellers_daily_goals
  has_many :sellers, through: :sellers_daily_goals

  validates :day, presence: true
  validates :seller_ids, presence: true
  validates :amount, daily_goal: true

  def amount_per_seller_to_s(amount_currency)
    return "" if !sellers.any?
    "#{DailyGoal.human_attribute_name(:sellers)}: #{sellers.pluck(:name).to_sentence} ( #{amount_currency} #{I18n.t(:for_each, scope:'helpers') if sellers.size > 1} )"
  end

  def amount_per_seller
    amount / sellers.size
  end

end
