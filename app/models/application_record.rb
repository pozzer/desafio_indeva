class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def amount=(attribute)
    if attribute && attribute.is_a?(String)
      write_attribute(:amount, attribute.gsub(".","").gsub(",","."))
    else
      write_attribute(:amount, attribute)
    end
  end
end
