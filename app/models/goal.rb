# == Schema Information
#
# Table name: goals
#
#  id         :integer          not null, primary key
#  month      :integer
#  beginning  :date
#  ending     :date
#  amount     :decimal(, )
#  store_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_goals_on_store_id  (store_id)
#

class Goal < ApplicationRecord
  belongs_to :store, required: true

  has_many :daily_goals

  validates :month, presence: true
  validates :beginning, presence: true
  validates :ending, presence: true
  validates :amount, presence: true
  validates :amount, numericality: { greater_than: 0 }
  validates :beginning, :ending, :overlap => {:scope => :store_id}

  after_save :create_daily_goals

  default_scope -> {includes(:daily_goals).order(:beginning)}

  def create_daily_goals
    daily_goals.destroy_all if daily_goals.any?
    (beginning..ending).each do |day|
      DailyGoal.new(day: day, goal: self).save(:validate => false)
    end
  end

  def month_to_s
    "#{I18n.t("date.month_names")[month]} / #{year}"
  end

  def year
    beginning.year
  end

end
