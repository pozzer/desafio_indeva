# == Schema Information
#
# Table name: stores
#
#  id         :integer          not null, primary key
#  name       :string
#  owner_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_stores_on_owner_id  (owner_id)
#

class Store < ApplicationRecord
  belongs_to :owner, required: true

  has_many :sellers, dependent: :destroy
  has_many :goals, dependent: :destroy

  accepts_nested_attributes_for :sellers, allow_destroy: true

  validates :name, presence: true
  validates :sellers, presence: true

  def to_s
    name
  end
end
