class DailyGoalValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if sum_other_daily_goals(record) + value > record.goal.amount
      record.errors.add(attribute, :higher_goal_sum)
    end
  end

  def sum_other_daily_goals(record)
    record.goal.daily_goals.where.not(id:record.id).sum(&:amount)
  end
end
