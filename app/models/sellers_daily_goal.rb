# == Schema Information
#
# Table name: sellers_daily_goals
#
#  id            :integer          not null, primary key
#  daily_goal_id :integer
#  seller_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_sellers_daily_goals_on_daily_goal_id  (daily_goal_id)
#  index_sellers_daily_goals_on_seller_id      (seller_id)
#

class SellersDailyGoal < ApplicationRecord
  belongs_to :seller
  belongs_to :daily_goal
end
