# == Schema Information
#
# Table name: goals
#
#  id         :integer          not null, primary key
#  month      :integer
#  beginning  :date
#  ending     :date
#  amount     :decimal(, )
#  store_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_goals_on_store_id  (store_id)
#

FactoryGirl.define do
  factory :goal do
    month 1
    beginning Date.new(2017,12,01)
    ending Date.new(2017,12,03)
    amount 1000.00
    store
  end
end
