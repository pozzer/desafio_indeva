# == Schema Information
#
# Table name: daily_goals
#
#  id         :integer          not null, primary key
#  day        :date
#  amount     :decimal(, )      default(0.0)
#  goal_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_daily_goals_on_goal_id  (goal_id)
#

FactoryGirl.define do
  factory :daily_goal do
    day Date.today
    amount 1000.00
    goal
  end
end
