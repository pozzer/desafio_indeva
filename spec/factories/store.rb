# == Schema Information
#
# Table name: stores
#
#  id         :integer          not null, primary key
#  name       :string
#  owner_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_stores_on_owner_id  (owner_id)
#

FactoryGirl.define do
  factory :store do
    name "Loja 1"
    owner

    transient do
      sellers_count 1
    end

    after :build do |store, evaluator|
      store.sellers << FactoryGirl.create_list(:seller, evaluator.sellers_count, store: store)
    end
  end
end
