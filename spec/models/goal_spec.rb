# == Schema Information
#
# Table name: goals
#
#  id         :integer          not null, primary key
#  month      :integer
#  beginning  :date
#  ending     :date
#  amount     :decimal(, )
#  store_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_goals_on_store_id  (store_id)
#

require 'rails_helper'

RSpec.describe Goal, type: :model do
  let(:goal) {FactoryGirl.build(:goal)}
  context "validations" do
    it "is valid with valid attributes" do
      expect(goal).to be_valid
    end

    it "is not valid without a month" do
      goal.month = nil
      expect(goal).to_not be_valid
    end

    it "is not valid without a beginning" do
      goal.beginning = nil
      expect(goal).to_not be_valid
    end

    it "is not valid without a ending" do
      goal.ending = nil
      expect(goal).to_not be_valid
    end

    it "is not valid without a store" do
      goal.store = nil
      expect(goal).to_not be_valid
    end

    it "is not valid without a amount" do
      goal.amount = nil
      expect(goal).to_not be_valid
    end

    it "is not valid amount equal zero" do
      goal.amount = 0
      expect(goal).to_not be_valid
    end

    it "is not valid amount less zero" do
      goal.amount = -1
      expect(goal).to_not be_valid
    end

    it "is not valid beginning and ending dates overlap" do
      goal.save
      goal2 = FactoryGirl.build(:goal, store: goal.store)
      expect(goal2).to_not be_valid
    end

  end

  describe ".year" do
    it "is year the beginning date" do
      expect(goal.year).to eq(goal.beginning.year)
    end
  end

  describe ".month_to_s" do
    it "is translated the month with o year" do
      expect(goal.month_to_s).to eq("#{I18n.t("date.month_names")[goal.month]} / #{goal.year}")
    end
  end

  describe "#after_save Callback" do
    it 'it "Creates the number of daily goals between the beginning and ending" do' do
      expect{ goal.save }.to change{ goal.daily_goals.count }.from(0).to((goal.beginning..goal.ending).to_a.size)
    end
  end

end
