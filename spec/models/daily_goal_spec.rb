# == Schema Information
#
# Table name: daily_daily_goals
#
#  id         :integer          not null, primary key
#  day        :date
#  amount     :decimal(, )      default(0.0)
#  goal_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_daily_goals_on_goal_id  (goal_id)
#

require 'rails_helper'

RSpec.describe DailyGoal, type: :model do
  let(:store) { FactoryGirl.build(:store) }
  let(:goal) { FactoryGirl.build(:goal, store: store) }
  let(:daily_goal) { FactoryGirl.build(:daily_goal, goal:goal, sellers: goal.store.sellers) }

  context "validations" do
    it "is valid with valid attributes" do
      expect(daily_goal).to be_valid
    end

    it "is not valid without a day" do
      daily_goal.day = nil
      expect(daily_goal).to_not be_valid
    end

    it "is not valid without a seller_ids" do
      daily_goal.seller_ids = []
      expect(daily_goal).to_not be_valid
    end

    it "is not valid if daily_goal amount more bigger amount goal" do
      daily_goal.amount = 10000.00
      expect(daily_goal).to_not be_valid
    end

    it "is not valid if sum daily_goals amount more bigger amount goal" do
      daily_goal2 =  FactoryGirl.create(:daily_goal, goal:goal, sellers: goal.store.sellers)
      expect(daily_goal).to_not be_valid
    end
  end

  describe "#amount_per_seller" do
    it "return blank if without sellers" do
      daily_goal.sellers = []
      expect(daily_goal.amount_per_seller_to_s("R$ 500,00")).to eq("")
    end

    it "return string with sellers and amount divided for each" do
      daily_goal.save
      expect(daily_goal.amount_per_seller_to_s("R$ 500,00")).to eq("#{DailyGoal.human_attribute_name(:sellers)}: #{daily_goal.sellers.pluck(:name).to_sentence} ( R$ 500,00  )")
    end
  end

  describe "#amount_per_seller" do
    it "return daily goal amount divided per sellers" do
      expect(daily_goal.amount_per_seller).to eq(daily_goal.amount / daily_goal.sellers.size )
    end
  end

end
