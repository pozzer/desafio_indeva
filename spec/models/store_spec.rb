# == Schema Information
#
# Table name: stores
#
#  id         :integer          not null, primary key
#  name       :string
#  owner_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_stores_on_owner_id  (owner_id)
#

require 'rails_helper'

RSpec.describe Store, type: :model do
  context "validations" do
    let(:store) { create(:store) }

    it "is valid with valid attributes" do
      expect(store).to be_valid
    end

    it "is not valid without a owner" do
      store.owner = nil
      expect(store).to_not be_valid
    end

    it "is not valid without a name" do
      store.name = nil
      expect(store).to_not be_valid
    end

    it "is not valid without a sellers" do
      store.sellers = []
      expect(store).to_not be_valid
    end
  end

  describe "#to_s" do
    let(:store) { FactoryGirl.build(:store) }
    it "return self name" do
      expect(store.to_s).to eq(store.name)
    end
  end
end
