require 'rails_helper'

RSpec.describe DailyGoalsController, type: :controller do
  let!(:owner){ create(:owner) }
  let!(:store){ create(:store, owner: owner) }
  let!(:goal){ create(:goal, store: store) }

  let!(:update_attributes) { {seller_ids: [store.sellers.pluck(:id).first]} }
  let!(:params) { { store_id: store.id, goal_id: goal.id } }
  let!(:attributes) { attributes_for(:daily_goal).merge(goal: goal, sellers: store.sellers) }
  let!(:resource){ create(:daily_goal, attributes) }

  before(:each){
    @request.env["devise.mapping"] = Devise.mappings[:daily_goal]
    sign_in owner
  }

  describe "GET #edit" do
    it "returns http success" do
      get :edit, params: params.merge({id: resource.to_param}), format: :js
      expect(response).to have_http_status(:success)
    end
  end

  describe "PUT #update" do
    it "returns http success" do
      put :update, params: params.merge({id: resource.id, :daily_goal => update_attributes}), format: :js
      expect(assigns(:daily_goal).public_send(update_attributes.keys.first)).to eq(update_attributes.values.first)
      expect(response).to have_http_status(:success)
    end
  end
end
