require 'rails_helper'

RSpec.describe GoalsController, type: :controller do
  let!(:owner){ create(:owner) }
  let!(:store){ create(:store, owner: owner) }
  let!(:update_attributes) { {month: 3} }
  let!(:search_ransack_params) { {} }
  let!(:params) { { store_id: store.id } }
  let!(:attributes) { attributes_for(:goal).merge(store: store) }
  let!(:create_attributes) { attributes.merge(beginning:Date.today, ending:Date.today+1.day) }

  it_behaves_like "SharedAppController"
end
