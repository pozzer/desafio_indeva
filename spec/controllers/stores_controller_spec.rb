require 'rails_helper'

RSpec.describe StoresController, type: :controller do
  let!(:owner){ create(:owner) }
  let!(:update_attributes) { {name: "olá"} }
  let!(:search_ransack_params) { {name_cont: "loja"} }
  let!(:params) { {} }
  let!(:attributes) { attributes_for(:store).merge(owner_id: owner.id, sellers_attributes: { "1": {name: "João"} } ) }
  let!(:create_attributes) { attributes }

  it_behaves_like "SharedAppController"
end
