require 'rails_helper'

RSpec.shared_examples "SharedAppController" do

  let!(:resource_class){ described_class.resource_class }
  let!(:resource_name){ described_class.resource_class.to_s.parameterize }
  let!(:resource){ create(resource_name, attributes) }

  before(:each){
    @request.env["devise.mapping"] = Devise.mappings[resource_name]
    sign_in owner
  }

  describe "GET #index" do
    it "returns http success" do
      get :index, params: params
      expect(response).to have_http_status(:success)
    end

    it "search and returns http success" do
      get :index, params: params.merge({q: search_ransack_params })
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #new" do
    it "returns http success" do
      get :new, params: params
      expect(assigns(resource_name)).to be_a_new(resource_class)
    end
  end

  describe "POST #create" do
    it "returns http success" do
      expect { post :create, params: params.merge({resource_name => create_attributes}) }.to  change(resource_class, :count)
      expect(response).to have_http_status(:redirect)
    end
  end

  context "Resource" do
    describe "GET #show" do
      it "returns http success" do
        get :show, params: params.merge({id: resource.to_param})
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #edit" do
      it "returns http success" do
        get :edit, params: params.merge({id: resource.to_param})
        expect(response).to have_http_status(:success)
      end
    end

    describe "PUT #update" do
      it "returns http success" do
        put :update, params: params.merge({id: resource.id, resource_name => update_attributes})
        expect(assigns(resource_name).public_send(update_attributes.keys.first)).to eq(update_attributes.values.first)
        expect(response).to have_http_status(:redirect)
      end
    end

    describe "DELETE #destroy" do
      it "returns http success" do
        expect { delete :destroy, params: params.merge({id: resource.to_param}) }.to change(resource_class, :count)
        expect(response).to have_http_status(:redirect)
      end
    end
  end

end
